$(document).ready(function(){
	
	$('.frock_question').click(function() {
    
    	$(this).closest('.frock_faq').find('.frock_answer').toggle('slow');
    
  	});

  	/*================================= 
	MAILCHIMP AJAX
	=================================*/

	// Set up form variables

	var mailchimpForm = $('.mc-embedded-subscribe-form');

	// On submit of the form send an ajax request to mailchimp for data.

	mailchimpForm.submit(function(e){

		// Set variables for this specific form
		
		var that = $(this);
		var mailchimpSubmit = $(this).find('input[type=submit]');
		var errorResponse = $(this).closest('.mc_embed_signup').find('.mce-error-response');
		var successResponse = $(this).closest('.mc_embed_signup').find('.mce-success-response');

		// Make sure the form doesn't link anywhere on submit.

		e.preventDefault();

		// JQuery AJAX request http://api.jquery.com/jquery.ajax/

		$.ajax({
		  method: 'GET',
		  url: that.attr('action'),
		  data: that.serialize(),
		  dataType: 'jsonp',
		  success: function(data) {
			// If there was an error then show the error message.
			if (data.result === 'error') {
				// Hide the first few characters int the error message string which display the error code and hyphen.
				var messageWithoutCode = data.msg.slice(3);
				errorResponse.text(messageWithoutCode).show(300).delay(3000).hide(300);
			}
			// If success then show message
			else {
				successResponse.text('Success! Please check your email for a confirmation message.').show(300).delay(3000).hide(300);
			}
		  }
		});
	});

});